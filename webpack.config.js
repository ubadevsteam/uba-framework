const 

    /**
     * Pasta aonde esta localizado o arquivo de entrada
     */
    entry_path              = './resources/ts/',

    /**
     * Pasta de saida dos arquivos após a compilação
     */
    output_path             = './public',
    
    /**
     * Pasta de saida do js e css, relativa a pasta de saida, caso não seja definida, a pasta de saida será a designada
     */
    js_path                 = './js/',
    css_path                = './css/',

    /**
     * Pasta aonde esta localizado o arquivo de ambiente
     */
    env_path                = entry_path,

    /**
     * Bibliotecas
     */
    path                    = require('path'),
    Dotenv                  = require('dotenv-webpack');
    ExtractTextPlugin       = require('extract-text-webpack-plugin'),
    webpack                 = require('webpack'),
    TerserPlugin            = require('terser-webpack-plugin'),
    glob                    = require('glob'),
    { CleanWebpackPlugin }  = require('clean-webpack-plugin');

/**
 * Busca todos os pontos de entrada para criaçando do lazy load
 */
const entry = function () {

    let entry_list = {};

    const src_files = glob.sync(path.resolve(entry_path, 'controllers/', '*.ts')); 

    src_files.map(function (file) {

        let filename = path.parse(file).name.toLocaleLowerCase();

        entry_list[filename] = file;
    });
    
    return Object.assign({
        bundle : entry_path + 'app.ts'
    }, entry_list)
}

module.exports = function (env, argv) {

    const 

        /**
         * Modo de compilação: Desenvolvimento ou Produção
         */
        mode    = argv.mode || 'development',

        /**
         * sufixo para o modo de desenvolvimento
         */
        suffix  = mode === 'development' ? '.dev' : '';

    return {
        watch   : mode === 'development',
        target  : 'web',
        mode    : mode,
        entry   : entry,
        output  : {
            filename    : path.join(js_path, `[name]${suffix}.js`),
            path        : path.resolve(__dirname, output_path)
        },
        module  : {
            rules: [
                /**
                 * Ejs
                 */
                {   
                    test: /\.ejs$/, 
                    loader: 'ejs-loader' 
                },
                /**
                 * Babel
                 */
                {
                    test    : /\.(ts|tsx)$/,
                    loader  : 'babel-loader'
                },
                /**
                 * Sass e Css
                 */
                {
                    test        : /\.scss$/,
                    use         : ExtractTextPlugin.extract({
                    fallback    : 'style-loader',
                    use         : [
                        { 
                            loader: 'css-loader?url=false'
                        },
                        {
                            loader : 'sass-loader'
                        }
                    ]
                })
                }
            ]
        },
        plugins : [
            new webpack.DefinePlugin({
                'process.env' : {
                    'JS_PATH'   : JSON.stringify(js_path),
                    'MODE'      : JSON.stringify(mode) 
                }
            }),
            new ExtractTextPlugin({
                filename : path.join(css_path, `[name]${suffix}.css`)
            }),
            new Dotenv({
                path : path.resolve(__dirname, env_path + '.env')
            }),
            new CleanWebpackPlugin({
                cleanOnceBeforeBuildPatterns : [
                    path.resolve(__dirname, output_path, js_path),
                    path.resolve(__dirname, output_path, css_path)
                ]
            })
        ],
        resolve : {
            extensions : [ 
                '.tsx', 
                '.ts',
                '.jsx', 
                '.js',
                '.json'
            ],
            alias : {
                "@sass" : path.resolve(__dirname, "resources/sass"),
                "@ts"   : path.resolve(__dirname, "resources/ts")
            }
        },
        optimization : {
            minimizer : [
                new TerserPlugin({
                    terserOptions : {
                        output : {
                            comments : false
                        }
                    }
                })
            ]
        }
    };
};