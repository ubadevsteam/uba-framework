import { Controller } from 'ubapackage';

class Example2 extends Controller {

    private init:string     = 'INIT';
    private ready:string    = 'READY';
    private load:string     = 'LAOD';

    constructor() {

        super();
        
        super.loader('Example2')
    }

    onInit(params?:object) {

        console.log('Init 4', this.init, params);
    }

    onReady(params?:object) {
        
        console.log('onReady 5', this.ready, params);
    }

    onLoad(params?:object) {
        
        console.log('onLoad 6', this.load, params);
    }
}

export default new Example2();                                                           
