import { Controller } from 'ubapackage';

class Example extends Controller {

    private init:string     = 'INIT';
    private ready:string    = 'READY';
    private load:string     = 'LAOD';

    constructor() {

        super();

        super.loader('Example')
    }

    onInit(params?:object) {

        console.log('Init 1', this.init, params);
    }

    onReady(params?:object) {
        
        console.log('onReady 2', this.ready, params);
    }

    onLoad(params?:object) {
        
        console.log('onLoad 3', this.load, params);
    }
}

export default new Example();                                                           
