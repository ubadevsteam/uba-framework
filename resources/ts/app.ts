/**
 * Sass
 */
import '@sass/app.scss';

/**
 * Libs
 */
import { Lazy } from 'ubapackage';

/**
 * Generals
 */
import Header from '@ts/generals/Header';
import Footer from '@ts/generals/Footer';
import Menu from '@ts/generals/Menu';

new Lazy().generals({
    Header,
    Footer,
    Menu
});